#include<sys/stat.h>
#include<sys/types.h>
#include<vector>
#include<string>
#include<algorithm>
#include<iostream>
#include<boost/filesystem.hpp>
#include<time.h>

extern "C" {
__declspec(dllexport) void syncing(char* PathLeft, char* PathRight, bool copyRight1, bool nothing1, bool updateRight2, bool nothing2, bool updateLeft3, bool nothing3, bool copyLeft4, bool nothing4, bool recycle);
__declspec(dllexport) int getNum(char* Path);
}

namespace bfs = boost::filesystem;

void syncing(char* PathLeft, char* PathRight, bool copyRight1, bool nothing1, bool updateRight2, bool nothing2, bool updateLeft3, bool nothing3, bool copyLeft4, bool nothing4, bool recycle);

std::vector<std::string> getAllFilesInDir(const std::string &dirPath, const std::vector<std::string> dirSkipList = {});

int getNum(char* Path);

void comp(const std::vector<std::string>& left, const std::vector<std::string>& right,
	std::vector<std::string>& diffl, std::vector<std::string>& diffr, std::vector<std::string>& both);


int main()
{
	char* PathLeft{ "C:/Users/AmirHo3ein/Desktop/test ap/dir 1" };
	char* PathRight{ "C:/Users/AmirHo3ein/Desktop/test ap/dir 2" };
	bool copyRight1{ true }, nothing1{ false }, updateRight2{ true }, nothing2{ false }, updateLeft3{ true }, nothing3{ false }, copyLeft4{ false }, nothing4{ false };
	bool recycle{ true };
	syncing(PathLeft, PathRight, copyRight1, nothing1, updateRight2, nothing2, updateLeft3, nothing3, copyLeft4, nothing4, recycle);
}



void syncing(char* PathLeft, char* PathRight, bool copyRight1, bool nothing1, bool updateRight2, bool nothing2, bool updateLeft3, bool nothing3, bool copyLeft4, bool nothing4, bool recycle)
{
	// Converting char* to string
	std::string dirPathLeft{ static_cast<std::string>(PathLeft) };
	std::string dirPathRight{ static_cast<std::string>(PathRight) };
	std::string Recycle{ "/Recycle" };

	if (recycle) {
		if (bfs::is_directory(dirPathLeft + Recycle))
			bfs::remove_all(dirPathRight + Recycle);
		if (bfs::is_directory(dirPathLeft + Recycle))
			bfs::remove_all(dirPathRight + Recycle);
		bfs::create_directory(dirPathLeft + Recycle);
		bfs::create_directory(dirPathRight + Recycle);
	}
	// Get recursive list of files in given directory and its sub directories
	std::vector<std::string> listOfLeft = getAllFilesInDir(dirPathLeft);
	std::vector<std::string> listOfRight = getAllFilesInDir(dirPathRight);

	std::vector<std::string> onlyLeft;
	std::vector<std::string> onlyRight;
	std::vector<std::string> both;

	comp(listOfLeft, listOfRight, onlyLeft, onlyRight, both);

	//the syncing process
	//first copy the onlyLeft and onlyRight in the other one
	if (!nothing1) {
		for (auto str : onlyLeft) {
			if (copyRight1) {
				if (bfs::is_directory(dirPathLeft + str))
					bfs::create_directories(dirPathRight + str);
				else if (bfs::is_regular_file(dirPathLeft + str))
					bfs::copy_file(dirPathLeft + str, dirPathRight + str);
			}
			else {
				if (recycle) {
					if (bfs::is_directory(dirPathLeft + str))
						bfs::create_directories(dirPathLeft + Recycle + str);
					else if (bfs::is_regular_file(dirPathLeft + str)) {
						std::string key{ "\\" };
						std::string folder{ str };
						size_t found{ str.rfind(key) };
						folder.erase(folder.begin() + found, folder.end());
						if ((folder != "") && !(bfs::is_directory(dirPathLeft + Recycle + folder)))
							bfs::create_directories(dirPathLeft + Recycle + folder);
						bfs::copy_file(dirPathLeft + str, dirPathLeft + Recycle + str);
					}
				}
			}
		}
		for (auto str : onlyLeft) {
			if (!copyRight1) {
				if (bfs::is_directory(dirPathLeft + str))
					bfs::remove_all(dirPathLeft + str);
				else if ((bfs::exists(dirPathLeft + str)) && (bfs::is_regular_file(dirPathLeft + str)))
					bfs::remove(dirPathLeft + str);
			}
		}
	}
	if (!nothing4) {
		for (auto str : onlyRight) {
			if (copyLeft4) {
				if (bfs::is_directory(dirPathRight + str))
					bfs::create_directories(dirPathLeft + str);
				else if (bfs::is_regular_file(dirPathRight + str))
					bfs::copy_file(dirPathRight + str, dirPathLeft + str);
			}
			else {
				if (recycle) {
					if (bfs::is_directory(dirPathRight + str))
						bfs::create_directories(dirPathRight + Recycle + str);
					else if (bfs::is_regular_file(dirPathRight + str))
					{
						std::string key{ "\\" };
						std::string folder{ str };
						size_t found{ str.rfind(key) };
						folder.erase(folder.begin() + found, folder.end());
						if ((folder != "") && !(bfs::is_directory(dirPathRight + Recycle + folder)))
							bfs::create_directories(dirPathRight + Recycle + folder);
						bfs::copy_file(dirPathRight + str, dirPathRight + Recycle + str);
					}
				}
			}
		}
		for (auto str : onlyRight) {
			if (!copyLeft4) {
				if (bfs::is_directory(dirPathRight + str))
					bfs::remove_all(dirPathRight + str);
				else if ((bfs::exists(dirPathRight + str)) && (bfs::is_regular_file(dirPathRight + str)))
					bfs::remove(dirPathRight + str);
			}
		}
	}
	if (!(nothing2 && nothing3)) {
		for (auto str : both) {
			if (bfs::is_regular_file(dirPathRight + str)) {
				time_t timeRight{ bfs::last_write_time(dirPathRight + str) };
				time_t timeLeft{ bfs::last_write_time(dirPathLeft + str) };
				if (difftime(timeRight, timeLeft) > 0.0) {
					if (!nothing3) {
						if (updateLeft3) {
							if (recycle) {
								std::string key{ "\\" };
								std::string folder{ str };
								size_t found{ str.rfind(key) };
								folder.erase(folder.begin() + found, folder.end());
								if ((folder != "") && !(bfs::is_directory(dirPathLeft + Recycle + folder)))
									bfs::create_directories(dirPathLeft + Recycle + folder);
								bfs::copy_file(dirPathLeft + str, dirPathLeft + Recycle + str);
							}
							bfs::copy_file(dirPathRight + str, dirPathLeft + str, bfs::copy_option::overwrite_if_exists);
						}
						else {
							if (recycle) {
								std::string key{ "\\" };
								std::string folder{ str };
								size_t found{ str.rfind(key) };
								folder.erase(folder.begin() + found, folder.end());
								if ((folder != "") && !(bfs::is_directory(dirPathRight + Recycle + folder)))
									bfs::create_directories(dirPathRight + Recycle + folder);
								bfs::copy_file(dirPathRight + str, dirPathRight + Recycle + str);
							}
							bfs::copy_file(dirPathLeft + str, dirPathRight + str, bfs::copy_option::overwrite_if_exists);
						}
					}
				}
				else {
					if (!nothing2) {
						if (updateRight2) {
							if (recycle) {
								std::string key{ "\\" };
								std::string folder{ str };
								size_t found{ str.rfind(key) };
								folder.erase(folder.begin() + found, folder.end());
								if ((folder != "") && !(bfs::is_directory(dirPathRight + Recycle + folder)))
									bfs::create_directories(dirPathRight + Recycle + folder);
								bfs::copy_file(dirPathRight + str, dirPathRight + Recycle + str);
							}
							bfs::copy_file(dirPathLeft + str, dirPathRight + str, bfs::copy_option::overwrite_if_exists);
						}
						else {
							if (recycle) {
								std::string key{ "\\" };
								std::string folder{ str };
								size_t found{ str.rfind(key) };
								folder.erase(folder.begin() + found, folder.end());
								if ((folder != "") && !(bfs::is_directory(dirPathLeft + Recycle + folder)))
									bfs::create_directories(dirPathLeft + Recycle + folder);
								bfs::copy_file(dirPathLeft + str, dirPathLeft + Recycle + str);
							}
							bfs::copy_file(dirPathRight + str, dirPathLeft + str, bfs::copy_option::overwrite_if_exists);
						}
					}
				}
			}
		}
	}
}

std::vector<std::string> getAllFilesInDir(const std::string &dirPath, const std::vector<std::string> dirSkipList)
{

	// Create a vector of string
	std::vector<std::string> listOfFiles;
	try {
		// Check if given path exists and points to a directory
		if (bfs::exists(dirPath) && bfs::is_directory(dirPath))
		{
			// Create a Recursive Directory Iterator object and points to the starting of directory
			bfs::recursive_directory_iterator iter(dirPath);

			// Create a Recursive Directory Iterator object pointing to end.
			bfs::recursive_directory_iterator end;

			// Iterate till end
			while (iter != end)
			{

				// Check if current entry is a directory and if exists in skip list
				if (bfs::is_directory(iter->path()) &&
					(std::find(dirSkipList.begin(), dirSkipList.end(), iter->path().filename()) != dirSkipList.end()))
				{
					// Skip the iteration of current directory pointed by iterator

					// c++17 Filesystem API to skip current directory iteration
					iter.disable_recursion_pending();
				}
				else
				{
					// Add the name in vector
					auto temp = (iter->path().string());
					listOfFiles.push_back(temp.erase(0, dirPath.length()));
				}

				boost::system::error_code ec;
				// Increment the iterator to point to next entry in recursive iteration
				iter.increment(ec);
				if (ec) {
					std::cerr << "Error While Accessing : " << iter->path().string() << " :: " << ec.message() << '\n';
				}
			}
		}
	}
	catch (std::system_error & e)
	{
		std::cerr << "Exception :: " << e.what();
	}
	return listOfFiles;
}


void comp(const std::vector<std::string>& left, const std::vector<std::string>& right,
	std::vector<std::string>& diffl, std::vector<std::string>& diffr, std::vector<std::string>& both)
{
	bool flag = true;
	for (size_t i = 0; i < left.size(); i++)
	{
		flag = true;
		for (size_t j = 0; j < right.size(); j++)
		{
			if (left[i] == right[j])
			{
				both.push_back(left[i]);
				flag = false;
				break;
			}
		}
		if (flag == true)
		{
			diffl.push_back(left[i]);
		}
	}
	for (size_t i = 0; i < right.size(); i++)
	{
		flag = true;
		for (size_t j = 0; j < left.size(); j++)
		{
			if (right[i] == left[j])
			{
				flag = false;
				break;
			}
		}
		if (flag == true)
		{
			diffr.push_back(right[i]);
		}
	}
}

int getNum(char* Path)
{
	std::string dirPath{ static_cast<std::string>(Path) };
	std::vector<std::string> list = getAllFilesInDir(dirPath);
	int Count{ static_cast<int>(list.size()) };
	return Count;
}
