import sys
import os, shutil
from PyQt5 import uic,QtCore, QtGui
from PyQt5.QtWidgets import QApplication,\
    QMenu, QAction , QMainWindow, QFileSystemModel,\
    QFileDialog, QTableWidgetItem, QAbstractItemView
from PyQt5.QtGui import QIcon, QStandardItem
from PyQt5.QtCore import QSize, QDir,QThread
from time import sleep
##dll function
import ctypes as ct
import numpy as np

#dll_path = (r'C:\Users\yh\Downloads\FileSys (2).dll')
dll_path = (os.path.join(os.getcwd(),'FileSys.dll'))
if not os.path.exists(dll_path):
    print("path does not exist")
    sys.exit(1)

#ct = np.ctypeslib.ct  ##using np
dll = ct.cdll.LoadLibrary(dll_path)
syncing= dll.syncing

getNum = dll.getNum
getNum.argtypes =[ct.c_char_p]
getNum.restypes = ct.c_int32
syncing.argtypes = [(ct.c_char_p), (ct.c_char_p), ct.c_bool, ct.c_bool, ct.c_bool, ct.c_bool, ct.c_bool, ct.c_bool,
                ct.c_bool, ct.c_bool, ct.c_bool]
syncing.restypes = None



recycle = True
backUp_recycle = True

copyRight1 = True
nothing1 = False

updateRight2 = True
nothing2 = False

updateLeft3 = True
nothing3 = False

copyLeft4 = True
nothing4 = False

backUp_copyRight1 = True
backUp_nothing1 = True

backUp_updateRight2 = True
backUp_nothing2 = True

backUp_updateLeft3 = True
backUp_nothing3 = True

backUp_copyLeft4 = True
backUp_nothing4 = True

count1 = 0
count2 = 0
count3 = 1
count4 = 1

backUp_count1 = 0
backUp_count2 = 0
backUp_count3 = 0
backUp_count4 = 0



Form = uic.loadUiType(os.path.join(os.getcwd(),'update1_2.ui'))[0]
Form2 = uic.loadUiType(os.path.join(os.getcwd(),'popup1.ui'))[0]
Form_set = uic.loadUiType(os.path.join(os.getcwd(),'popup_set.ui'))[0]

#The Main Class


class showing (Form, QMainWindow):
    def __init__(self):
        Form.__init__(self)
        QMainWindow.__init__(self)
        self.setupUi(self)

        ##the lists needed for getting directories
        #taking out every file and folder in the path
        self.rightFiles = []
        self.rightSubdirs = []
        self.leftFiles = []
        self.leftSubdirs = []
        #all the files and folders in the path
        self.left_all = []
        self.right_all = []
        self.file1 = ''
        self.file2 = ''

        global recycle
        global count1
        global count2
        global count3
        global count4
        global updateRight2
        global nothing2
        global updateLeft3
        global nothing3
        global copyRight1
        global nothing1
        global copyLeft4
        global nothing4

        self.recycle = recycle
        self.copyRight1 = copyRight1
        self.nothing1 = nothing1
        self.updateRight2 = updateRight2
        self.nothing2 = nothing2
        self.updateLeft3 = updateLeft3
        self.nothing3 = nothing3
        self.copyLeft4 = copyLeft4
        self.nothing4 = nothing4
        self.count1 = count1
        self.count2 = count2
        self.count3 = count3
        self.count4 = count4

        #thread
        self.syncingThread = None

        self.NewButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/new.png")))
        self.NewButton.setIconSize(QSize(52, 52))
        self.NewButton.clicked.connect(self.newCall)
        self.OpenButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/open.png")))
        self.OpenButton.setIconSize(QSize(52, 52))
        self.OpenButton.clicked.connect(self.openCall)
        self.SaveButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/Save.png")))
        self.SaveButton.setIconSize(QSize(52, 52))
        self.SaveButton.clicked.connect(self.saveCall)
        self.SaveAsButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/SaveAs.png")))
        self.SaveAsButton.setIconSize(QSize(52, 52))
        self.SaveAsButton.clicked.connect(self.saveCall)
        self.compareButton.setIcon(QtGui.QIcon(os.path.join(os.getcwd(), "Icons/compare")))
        self.compareButton.setIconSize(QtCore.QSize(100, 100))
        self.compareButton.setStyleSheet(' font: bold 14px ; height:10px; ')
        self.compareButton.setToolTip("Start comparison")
        self.settingButton.setIcon(QtGui.QIcon(os.path.join(os.getcwd(), "Icons/cfg_compare")))
        self.settingButton.setIconSize(QtCore.QSize(100, 100))
        self.settingButton.clicked.connect(self.open_new_dialog_setting)
        self.settingButton.setToolTip("Comparison settings")
        self.filterButton.setIcon(QtGui.QIcon(os.path.join(os.getcwd(), "Icons/cfg_filter")))
        self.filterButton.setIconSize(QtCore.QSize(100, 100))
        self.filterButton.clicked.connect(self.open_new_dialog_setting)
        self.filterButton.setToolTip("Filter")
        self.syncButton.setIcon(QtGui.QIcon(os.path.join(os.getcwd(), "Icons/cfg_sync")))
        self.syncButton.setIconSize(QtCore.QSize(100, 100))
        self.syncButton.clicked.connect(self.open_new_dialog_setting)
        self.syncButton.setToolTip("Synchronization settings")
        self.syncButtonBig.setIcon(QtGui.QIcon(os.path.join(os.getcwd(), "Icons/file_sync")))
        self.syncButtonBig.setIconSize(QtCore.QSize(100, 100))
        self.syncButtonBig.setToolTip("Start synchronization")
        self.syncButtonBig.setStyleSheet('QPushButton { font: bold 14px;}')

        self.menu = QMenu()
        Action_1 = QAction(QIcon('Icons/cmp_file_time_sicon.png'), '&File time and size', self)
        Action_2 = QAction(QIcon('Icons/status_binary_compare.png'), '&File content', self)
        Action_3 = QAction(QIcon('Icons/cmp_file_size_sicon.png'), '&File size', self)
        self.menu.addAction(Action_1)
        self.menu.addAction(Action_2)
        self.menu.addAction(Action_3)
        self.menu.setDisabled(True)
        self.leftSettingTool.setMenu(self.menu)
        self.leftSettingTool.clicked.connect(self.leftSettingTool.showMenu)

        self.menu = QMenu()
        self.menu.addAction('Clear filter')
        self.menu.addAction('Copy')
        self.menu.addAction('Paste')
        self.menu.setDisabled(True)
        self.filterButtonTool.setMenu(self.menu)
        self.filterButtonTool.clicked.connect(self.filterButtonTool.showMenu)

        self.menu = QMenu()
        Act_1 = self.menu.addAction('<- Two way ->')
        Act_2 = self.menu.addAction('Mirror ->')
        Act_3 = self.menu.addAction('Update >')
        Act_4 = self.menu.addAction('Custom')
        self.object = NewDialog_setting(self)
        Act_1.triggered.connect(self.object.Two)
        Act_2.triggered.connect(self.object.Mirror)
        Act_3.triggered.connect(self.object.Update)
        Act_4.triggered.connect(self.object.Custom)
        self.rightSettingTool.setMenu(self.menu)
        self.rightSettingTool.clicked.connect(self.rightSettingTool.showMenu)
        self.SwapButton.setIcon(QtGui.QIcon(os.path.join(os.getcwd(), "Icons/swap")))
        self.SwapButton.setIconSize(QtCore.QSize(100, 100))
        self.SwapButton.clicked.connect(self.SwapDirs)
        self.addFolderPair.setIcon(QtGui.QIcon(os.path.join(os.getcwd(), "Icons/item_add")))
        self.addFolderPair.setIconSize(QtCore.QSize(100, 100))
        self.addFolderPair.setToolTip('add Folder Pair')
        self.comboLeft.setEditable(True)
        self.comboLeft.lineEdit().setMaxLength(90)
        self.comboRight.setEditable(True)
        self.comboRight.lineEdit().setMaxLength(90)
        #compare
        self.compareButton.clicked.connect(self.list)
        #syncing
        self.syncButtonBig.clicked.connect(self.sync)
        #browsing directory
        self.browseButtonLeft.setToolTip("Select a folder")
        self.browseButtonRight.setToolTip("Select a folder")
        self.browseButtonLeft.clicked.connect(self.BrowseLeft)
        self.browseButtonRight.clicked.connect(self.BrowseRight)

        # Create new action
        newAction = QAction(QIcon('Icons/new.png'), '&New', self)
        newAction.setShortcut('Ctrl+N')
        newAction.setStatusTip('New document')
        newAction.triggered.connect(self.newCall)

        # Create new action
        openAction = QAction(QIcon('Icons/open.png'), '&Open', self)
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open document')
        openAction.triggered.connect(self.openCall)

        # Create exit action
        exitAction = QAction(QIcon('Icons/exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.exitCall)

        saveAction = QAction(QIcon('Icons/save.png'), '&Save', self)
        saveAction.setShortcut('Ctrl+S')
        saveAction.setStatusTip('Save application')
        saveAction.triggered.connect(self.exitCall)

        # Create menu bar and add action

        self.menuFile.addAction(newAction)
        self.menuFile.addAction(openAction)
        self.menuFile.addAction(saveAction)
        self.menuFile.addAction(exitAction)



        # Create new action
        newAction = QAction(QIcon('Icons/compare.png'), '&Start comparison', self)
        newAction.setShortcut('F5')
        newAction.triggered.connect(self.open_new_dialog_compare)

        # Create new action
        openAction = QAction(QIcon('Icons/cfg_compare.png'), '&Comparison settings', self)
        openAction.setShortcut('F6')
        openAction.triggered.connect(self.open_new_dialog_setting)

        # Create exit action
        exitAction = QAction(QIcon('Icons/cfg_filter.png'), '&Filter settings', self)
        exitAction.setShortcut('F7')
        exitAction.triggered.connect(self.open_new_dialog_setting)

        saveAction = QAction(QIcon('Icons/cfg_sync.png'), '&Synchronization settings', self)
        saveAction.setShortcut('F8')
        saveAction.triggered.connect(self.open_new_dialog_setting)

        # Create menu bar and add action
        self.menuActions.addAction(newAction)
        self.menuActions.addAction(openAction)
        self.menuActions.addAction(saveAction)
        self.menuActions.addAction(exitAction)

    def openCall(self):
        startingDir = r'C:\\'
        file = QFileDialog.getExistingDirectory(self, "Select Directory", startingDir)

    def newCall(self):
        startingDir = r'C:\\'
        file = QFileDialog.getExistingDirectory(self, "Select Directory", startingDir)

    def saveCall(self):
        startingDir = r'C:\\'
        file = QFileDialog.getExistingDirectory(self, "Select Directory", startingDir)

    def exitCall(self):
        self.close()



    def open_new_dialog_compare(self):
        self.nd = NewDialog_compare(self)
        self.nd.show()

    def open_new_dialog_setting(self):
        self.nd = NewDialog_setting(self)
        self.nd.show()

    def BrowseLeft(self):
        startingDir = r'C:\\'
        self.file1 = QFileDialog.getExistingDirectory(self, "Select Directory",startingDir)
        self.comboLeft.lineEdit().setText(self.file1)
        self.dllfile1 = ct.create_string_buffer(bytes(self.file1, encoding='utf-8'))
        self.leftFiles = []
        self.leftSubdirs = []
        self.left_all = []



    def BrowseRight(self):
        startingDir = r'C:\\'
        self.file2 = str(QFileDialog.getExistingDirectory(self, "Select Directory",startingDir))
        self.comboRight.lineEdit().setText(self.file2)
        ##dll adding
        self.dllfile2 = ct.create_string_buffer(bytes(self.file2, encoding='utf-8'))
        self.right_all = []
        self.rightFiles = []
        self.rightSubdirs = []

    def SwapDirs(self):
        temp = self.file1
        self.file1 = self.file2
        self.file2 = temp
        self.comboLeft.lineEdit().setText(self.file1)
        self.comboRight.lineEdit().setText(self.file2)
        ##dll adding
        self.dllfile2 = ct.create_string_buffer(bytes(self.file2, encoding='utf-8'))
        self.dllfile1 = ct.create_string_buffer(bytes(self.file1, encoding='utf-8'))


    def list(self):

        self.right_all = []
        self.rightFiles = []
        self.rightSubdirs = []
        self.leftFiles = []
        self.leftSubdirs = []
        self.left_all = []

        text1 = self.comboLeft.currentText()
        text2 = self.comboRight.currentText()
        self.file2 = text2
        self.file1 = text1

        if text1:
            overviewList = [d for d in os.listdir(self.file1) if os.path.isdir(os.path.join(self.file1, d))]
            ovmodel = QFileSystemModel()
            ovmodel.setFilter(QDir.Dirs | QDir.NoDotAndDotDot | QDir.Files)
            ovmodel.setRootPath(self.file1)
            self.overviewTree.setModel(ovmodel)
            ovDir = QtCore.QDir(self.file1)
            self.overviewTree.setRootIndex(ovmodel.index(QtCore.QDir.path(ovDir), 0))
            self.overviewTree.setItemsExpandable(False)
            #main trees filled with files
            model = QFileSystemModel()
            model.setFilter(QDir.AllDirs | QDir.NoDotAndDotDot | QDir.AllEntries)
            model.setRootPath(self.file1)
            self.tree.setModel(model)
            self.tree.setAlternatingRowColors(True)
            setDir = QtCore.QDir(self.file1)
            self.tree.setRootIndex(model.index(QtCore.QDir.path(setDir), 0))

            #right list
            model = QFileSystemModel()
            model.setFilter(QDir.AllDirs | QDir.NoDotAndDotDot | QDir.AllEntries)
            model.setRootPath(self.file2)
            self.tree_2.setModel(model)
            self.tree_2.setAlternatingRowColors(True)
            setDir = QtCore.QDir(self.file2)
            self.tree_2.setRootIndex(model.index(QtCore.QDir.path(setDir), 0))
            #under the left tree view
            self.leftLineEditText = str(getNum(self.dllfile1)) + " Files and Folders"
            self.leftLineEdit.setText(self.leftLineEditText)
            self.leftLineEdit.setReadOnly(True)
            self.leftLineEdit.setAlignment(QtCore.Qt.AlignCenter)
            #under the right tree view
            self.rightLineEditText = str(getNum(self.dllfile2)) + " Files and Folders"
            self.rightLineEdit.setText(self.rightLineEditText)
            self.rightLineEdit.setReadOnly(True)
            self.rightLineEdit.setAlignment(QtCore.Qt.AlignCenter)

        else :
            self.open_new_dialog_compare()

    def sync(self):
        self.syncingThread = WorkingThread(self.dllfile1,self.dllfile2)
        self.syncingThread.start()
        self.leftLineEditText = str(getNum(self.dllfile1)) + " Files and Folders"
        self.leftLineEdit.setText(self.leftLineEditText)
        self.leftLineEdit.setReadOnly(True)
        self.leftLineEdit.setAlignment(QtCore.Qt.AlignCenter)
        # under the right tree view
        self.rightLineEditText = str(getNum(self.dllfile2)) + " Files and Folders"
        self.rightLineEdit.setText(self.rightLineEditText)
        self.rightLineEdit.setReadOnly(True)
        self.rightLineEdit.setAlignment(QtCore.Qt.AlignCenter)


class NewDialog_compare(Form2,QMainWindow):
    def __init__(self, parent):
        super(NewDialog_compare, self).__init__(parent)
        Form2.__init__(self)
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.textEdit.setReadOnly(True)
        self.setWindowTitle("Warning")
        self.Cancel.clicked.connect(self.close)
        self.Ignore.clicked.connect(self.close)


class NewDialog_setting(Form_set,QMainWindow):
    def __init__(self, parent):
        super(NewDialog_setting, self).__init__(parent)
        Form_set.__init__(self)
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.setWindowTitle("Settings")
        global recycle
        global backUp_recycle
        global backUp_count1
        global backUp_count2
        global backUp_count3
        global backUp_count4
        global backUp_updateRight2
        global backUp_nothing2
        global backUp_updateLeft3
        global backUp_nothing3
        global backUp_copyRight1
        global backUp_nothing1
        global backUp_copyLeft4
        global backUp_nothing4

        backUp_recycle = recycle
        backUp_updateRight2 = updateRight2
        backUp_nothing2 = nothing2
        backUp_updateLeft3 = updateLeft3
        backUp_nothing3 = nothing3
        backUp_copyRight1 = copyRight1
        backUp_nothing1 = nothing1
        backUp_copyLeft4 = copyLeft4
        backUp_nothing4 = nothing4
        backUp_count1 = count1
        backUp_count2 = count2
        backUp_count3 = count3
        backUp_count4 = count4

        if recycle == True:
            self.checkBox.setChecked(True)

        self.count1 = count1
        self.count2 = count2
        self.count3 = count3
        self.count4 = count4

        self.textEdit.setText("\n\tIdentify equal files by comparing\n\n\tmodification time and sizefile")
        self.textEdit.setReadOnly(True)
        self.pushButton_5.clicked.connect(self.text1)
        self.pushButton.clicked.connect(self.text2)
        self.pushButton_2.clicked.connect(self.text3)

        self.pushButton_OK.clicked.connect(self.OK)
        self.pushButton_Cancel.clicked.connect(self.updateFlag)

        self.pushButton_Two.clicked.connect(self.Two)
        self.pushButton_Mirror.clicked.connect(self.Mirror)
        self.pushButton_Update.clicked.connect(self.Update)
        self.pushButton_Custom.clicked.connect(self.Custom)

        self.toolButton.setIconSize(QtCore.QSize(100, 100))
        self.toolButton.setToolTip("For files only in left directory")
        if count1 == 0:
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_right.png")))
        elif count1 == 1:
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_delete_left.png")))
        elif count1 == 2:
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))

        self.toolButton_2.setIconSize(QtCore.QSize(100, 100))
        self.toolButton_2.setToolTip("For newer files in left directory")
        if count2 == 0:
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
        elif count2 == 1:
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_left.png")))
        elif count2 == 2:
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))

        self.toolButton_3.setIconSize(QtCore.QSize(100, 100))
        self.toolButton_3.setToolTip("For newer files in right directory")
        if self.count3 == 0:
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
        elif self.count3 == 1:
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_left.png")))
        elif self.count3 == 2:
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))

        self.toolButton_4.setIconSize(QtCore.QSize(100, 100))
        self.toolButton_4.setToolTip("For files only in right directory")
        if self.count4 == 0:
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_delete_right.png")))
        elif self.count4 == 1:
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_left.png")))
        elif self.count4 == 2:
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))

        self.toolButton.clicked.connect(self.flag1)
        self.toolButton_2.clicked.connect(self.flag2)
        self.toolButton_3.clicked.connect(self.flag3)
        self.toolButton_4.clicked.connect(self.flag4)

    def text1(self):
        self.textEdit.setText("\n\tIdentify equal files by comparing\n\n\tmodification time and sizefile")

    def text2(self):
        self.textEdit.setText("\n\tIdentify equal files by comparing\n\n\tthe file content")

    def text3(self):
        self.textEdit.setText("\n\tIdentify equal files by comparing\n\n\ttheir file size")

    def OK(self):
        global recycle
        if self.checkBox.isChecked():
            recycle = True
        else:
            recycle = False
        self.close()

    def updateFlag(self):
        global recycle
        global count1
        global count2
        global count3
        global count4
        global updateRight2
        global nothing2
        global updateLeft3
        global nothing3
        global copyRight1
        global nothing1
        global copyLeft4
        global nothing4

        recycle = backUp_recycle
        updateRight2 = backUp_updateRight2
        nothing2 = backUp_nothing2
        updateLeft3 = backUp_updateLeft3
        nothing3 = backUp_nothing3
        copyRight1 = backUp_copyRight1
        nothing1 = backUp_nothing1
        copyLeft4 = backUp_copyLeft4
        nothing4 = backUp_nothing4
        count1 = backUp_count1
        count2 = backUp_count2
        count3 = backUp_count3
        count4 = backUp_count4

        self.close()

    def Two(self):
        self.textEdit_2.setText("\n\tBoth directories will be changed")
        self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_right.png")))
        self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
        self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_left.png")))
        self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_Left.png")))

        global count1
        global count2
        global count3
        global count4
        global updateRight2
        global nothing2
        global updateLeft3
        global nothing3
        global copyRight1
        global nothing1
        global copyLeft4
        global nothing4

        updateRight2 = True
        nothing2 = False
        updateLeft3 = True
        nothing3 = False
        copyRight1 = True
        nothing1 = False
        copyLeft4 = True
        nothing4 = False

        count1 = 0
        count2 = 0
        count3 = 1
        count4 = 1

    def Mirror(self):
        self.textEdit_2.clear()
        self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_right.png")))
        self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
        self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
        self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_delete_right.png")))

        global count1
        global count2
        global count3
        global count4
        global updateRight2
        global nothing2
        global updateLeft3
        global nothing3
        global copyRight1
        global nothing1
        global copyLeft4
        global nothing4

        updateRight2 = True
        nothing2 = False
        updateLeft3 = False
        nothing3 = False
        copyRight1 = True
        nothing1 = False
        copyLeft4 = False
        nothing4 = False

        count1 = 0
        count2 = 0
        count3 = 0
        count4 = 0

    def Update(self):
        self.textEdit_2.clear()
        self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_right.png")))
        self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
        self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))
        self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))
        global count1
        global count2
        global count3
        global count4
        global updateRight2
        global nothing2
        global updateLeft3
        global nothing3
        global copyRight1
        global nothing1
        global copyLeft4
        global nothing4

        updateRight2 = True
        nothing2 = False
        updateLeft3 = False
        nothing3 = True
        copyRight1 = True
        nothing1 = False
        copyLeft4 = False
        nothing4 = True
        count1 = 0
        count2 = 0
        count3 = 2
        count4 = 2


    def Custom(self):
        self.textEdit_2.clear()
        global count1
        global count2
        global count3
        global count4
        self.count1 = count1
        self.count2 = count2
        self.count3 = count3
        self.count4 = count4
        if self.count1 == 0:
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_right.png")))
        if self.count1 == 1:
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_delete_left.png")))
        if self.count1 == 2:
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))

        if self.count2 == 0:
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
        if count2 == 1:
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_left.png")))
        if count2 == 2:
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))

        if self.count3 == 0:
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
        if self.count3 == 1:
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_left.png")))
        if self.count3 == 2:
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))

        if self.count4 == 0:
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_delete_right.png")))
        if self.count4 == 1:
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_left")))
        if self.count4 == 2:
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))


    def flag1(self):
        global count1
        self.count1 = count1
        self.count1 = self.count1 + 1
        if self.count1 == 3:
            self.count1 = 0
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_right.png")))
            global copyRight1
            copyRight1 = True
            global nothing1
            nothing1 = False
        if self.count1 == 1:
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_delete_left.png")))
            copyRight1 = False
            nothing1 = False
        if self.count1 == 2:
            self.toolButton.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))
            copyRight1 = False
            nothing1 = True

        count1 = self.count1

    def flag2(self):
        global count2
        self.count2 =  count2
        self.count2 = self.count2 + 1
        if self.count2 == 3:
            self.count2 = 0
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
            global updateRight2
            global nothing2
            updateRight2 = True
            nothing2 = False
        if self.count2 == 1:
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_left.png")))
            updateRight2 = False
            nothing2 = False
        if self.count2 == 2:
            self.toolButton_2.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))
            updateRight2 = False
            nothing2 = True

        count2= self.count2
    def flag3(self):
        global count3
        self.count3 = count3
        self.count3 = self.count3 + 1
        if self.count3 == 3:
            self.count3 = 0
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_right.png")))
            global updateLeft3
            global nothing3
            updateLeft3 = False
            nothing3 = False
        if self.count3 == 1:
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_update_left.png")))
            updateLeft3 = True
            nothing3 = False
        if self.count3 == 2:
            self.toolButton_3.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))
            updateLeft3 = False
            nothing3 = True

        count3=self.count3

    def flag4(self):
        global count4
        self.count4= count4
        self.count4 = self.count4 + 1
        if self.count4 == 3:
            self.count4 = 0
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_delete_right.png")))
            global copyLeft4
            global nothing4
            copyLeft4 = False
            nothing4 = False
        if self.count4 == 1:
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_create_left")))
            copyLeft4 = False
            nothing4 = True
        if self.count4 == 2:
            self.toolButton_4.setIcon(QIcon(os.path.join(os.getcwd(), "Icons/so_none.png")))
            copyLeft4 = False
            nothing4 = True
        count4 = self.count4

class WorkingThread(QThread):
    def __init__(self, dl1, dl2):
        QThread.__init__(self)
        self.dllfile1 = dl1
        self.dllfile2 = dl2
    def run(self):
        syncing(self.dllfile1, self.dllfile2, copyRight1, nothing1, updateRight2, nothing2, updateLeft3, nothing3,
                copyLeft4, nothing4, recycle)




if __name__ == '__main__':

    app = QApplication (sys.argv)
    w=showing()
    w.show()
    sys.exit(app.exec_())
